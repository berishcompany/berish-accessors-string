export function setValue<T>(obj: any, path: string, value: T) {
    if (!path) return;
    let paths = path.split('.');
    let i = 0;
    for (i = 0; i < paths.length - 1; i++) {
      if (obj == null) return;
      obj = obj[paths[i]];
    }
    obj[paths[i]] = value;
  }
  
  export function getValue<T = any>(obj: any, path: string): T {
    if (!path) return obj;
    let paths = path.split('.');
    for (let i = 0; i < paths.length; i++) {
      if (obj == null) return null;
      obj = obj[paths[i]];
    }
    return obj;
  }
  